# Rust & GUI - egui

Live version: [g-braeunlich.gitlab.io/2021-10-07_rust-egui](https://g-braeunlich.gitlab.io/2021-10-07_rust-egui/)

## Use locally

One time dev setup:

```bash
# Download reveal.js:
yarn install
```

Then open [index.html](index.html) in a browser.
