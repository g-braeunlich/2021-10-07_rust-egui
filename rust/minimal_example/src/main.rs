use eframe::{egui, epi, NativeOptions};

#[derive(Default)]
pub struct App {}
impl epi::App for App {
    fn name(&self) -> &str {
        "Minimal Example"
    }
    fn update(&mut self, ctx: &egui::CtxRef, _frame: &mut epi::Frame<'_>) {
        egui::CentralPanel::default().show(ctx, |ui| {
            ui.label("Chuck Norris killed Bambi");
        });
    }
}

fn main() {
    let app = App::default();
    eframe::run_native(Box::new(app), NativeOptions::default());
}
