use eframe::{egui, epi, NativeOptions};

#[derive(Default)]
pub struct App {
    state_x: f64,
    state_y: f64,
}
impl epi::App for App {
    fn name(&self) -> &str {
        "State Handling"
    }
    fn update(&mut self, ctx: &egui::CtxRef, _frame: &mut epi::Frame<'_>) {
        egui::CentralPanel::default().show(ctx, |ui| {
            ui.label("x:");
            ui.add(egui::widgets::DragValue::new(&mut self.state_x));
            ui.label("y:");
            ui.add(egui::widgets::DragValue::new(&mut self.state_y));
        });
    }
}

fn main() {
    let app = App::default();
    eframe::run_native(Box::new(app), NativeOptions::default());
}
